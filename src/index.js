import React from "react";
import ReactDOM from "react-dom";

import "Styles/Main.scss";
import App from "./App";

import { createStore } from "redux";
import { Provider } from "react-redux";

const initialStore = {
  favPokemon: [],
};

const rootReducer = (state = initialStore, action) => {
  switch (action.type) {
    case "SET_FAV_POKEMON":
      return {
        ...state,
        favPokemon: state.favPokemon.concat(action.newValue),
      };
    default:
      break;
  }
  return state;
};

const storeRedux = createStore(rootReducer);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={storeRedux}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
