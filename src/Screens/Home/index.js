import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Layout from "Components/Layout";

class Home extends Component {
  constructor() {
    super();
    this.state = {
      pokemons: [],
      isLoading: true,
      limit: 10,
      offset: 0,
      favorites: [],
    };
  }

  componentDidMount() {
    this.fetchdataDenganFetch();
  }

  fetchdataDenganFetch = () => {
    const { limit, offset } = this.state;
    this.setState({ isLoading: true });
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`)
      .then((response) => response.json())
      .then((data) =>
        this.setState({ pokemons: data.results, isLoading: false })
      )
      .catch((error) => console.log(error));
  };

  render() {
    console.log(this.props);
    const { pokemons, isLoading, offset, favorites } = this.state;

    return (
      <Layout>
        <div className="home">
          <div className="home__title">Poke Favs</div>
          <div className="home__grid container">
            {this.props.favPokemon.length === 0 || isLoading
              ? null
              : this.props.favPokemon.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${pokemon.id}`}
                      >
                        <span>{pokemon.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>

          <div className="home__title">Poke Apps</div>
          <div className="home__grid container">
            {pokemons.length === 0 || isLoading
              ? null
              : pokemons.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <button
                        className="home__grid__item__save"
                        onClick={
                          () =>
                            this.props.handleAddFavPokemon({
                              newValue: {
                                id: offset + index + 1,
                                name: pokemon.name,
                              },
                            })
                          // this.setState({
                          //   favorites: favorites.concat({
                          //     id: offset === 0 ? index + 1 : offset + index + 1,
                          //     name: pokemon.name,
                          //   }),
                          // })
                        }
                      >
                        +
                      </button>

                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${offset + index + 1}`}
                      >
                        <span>{pokemon.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>
        </div>
      </Layout>
    );
  }
}

//mapping data dari state redux ke komponen yang menggunakan (terbaca sebagai props)
const mapStateToProps = (state) => {
  return {
    favPokemon: state.favPokemon,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAddFavPokemon: (params) =>
      dispatch({ type: "SET_FAV_POKEMON", newValue: params.newValue }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
